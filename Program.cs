﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace Action.DubbeleRecords
{
    class Program
    {

        static void Main(string[] args)
        {
            try
            {
                DateTime dtStart = Algemeen.Proces.dtStringToDate(args[0]);
                DateTime dtEind = Algemeen.Proces.dtStringToDate(args[1]);
                string sFil = "";

                if (args.Length >= 3)
                {
                    sFil = args[2];
                }
                Dictionary<string, List<string>> dslPers = Algemeen.Proces.DlsSqlSel("badnum, afdkod, wplekkod, naam, init, salnum from personeel");
                String sCreateBwCommand = "badnum, van_date, van_time, time, wplekkod, filler, actkod, recnr from proj_regi where plan = '1' and van_date >= '" + dtStart.ToString("yyMMdd") + "' and van_date <= '" + dtEind.ToString("yyMMdd") + "' and actkod < 100";
                List<string> lsArgs = new List<string>();
                lsArgs.Add("-d;");
                FileInfo fiBwFile = Algemeen.Proces.FiSqlsel(sCreateBwCommand, lsArgs);
                //zet bwfile om naar list
                List<List<string>> llsBwList = Algemeen.Proces.LlsCsvToList(fiBwFile);
                List<projRegi> lprList = new List<projRegi>();
                List<DateTimeRange> dtrList = new List<DateTimeRange>();
                foreach (List<string> lsBwRecord in llsBwList)
                {
                    if (lsBwRecord.Count > 7)
                    {
                        try
                        {
                            projRegi pr = new projRegi();
                            pr.sBadnum = lsBwRecord[0];
                            pr.dtDateTime = Algemeen.Proces.dtStringToDate(lsBwRecord[1]);
                            pr.dtDateTime = pr.dtDateTime.AddMinutes(Convert.ToDouble(lsBwRecord[2]));
                            pr.tsTime = new TimeSpan(0, Convert.ToInt32(lsBwRecord[3]), 0);
                            pr.sWplekkod = lsBwRecord[4];
                            if (lsBwRecord[5].Trim().Length > 0)
                            {
                                pr.tsPause = new TimeSpan(0, Convert.ToInt32(lsBwRecord[5]), 0);
                            }
                            else
                            {
                                pr.tsPause = new TimeSpan(0, 0, 0);
                            }
                            if (lsBwRecord[6].Trim().Length > 0)
                            {
                                pr.sActkod = lsBwRecord[6];
                            }
                            else
                            {
                                pr.sActkod = "";
                            }
                            pr.sRecNr = lsBwRecord[7];
                            DateTimeRange dtr = new DateTimeRange();
                            dtr.Start = pr.dtDateTime;
                            dtr.End = pr.dtDateTime + pr.tsPause + pr.tsTime;
                            dtr.sBadnum = pr.sBadnum;
                            dtr.sRecNr = pr.sRecNr;

                            dtrList.Add(dtr);
                            lprList.Add(pr);
                        }
                        catch (Exception ex)
                        {
                        }
                    }
                }
                List<string> lsOverlapNrs = new List<string>();
                List<Overlap> loOverlap = new List<Overlap>();
                foreach (DateTimeRange dtr in dtrList)
                {
                    foreach (DateTimeRange dtr2 in dtrList)
                    {
                        if (dtr.Intersects(dtr2) && (!lsOverlapNrs.Contains(dtr.sRecNr)))
                        {
                            Overlap o = new Overlap();
                            o.dtr1 = dtr;
                            o.dtr2 = dtr2;
                            lsOverlapNrs.Add(dtr.sRecNr);
                            lsOverlapNrs.Add(dtr2.sRecNr);
                            loOverlap.Add(o);
                        }
                    }
                }
                StreamWriter sw = new StreamWriter(Algemeen.Parameter.SSbRoot + "log\\Overlap.csv");
                foreach (Overlap o in loOverlap)
                {
                    List<string> lsPers = dslPers[o.dtr1.sBadnum];
                    if (sFil.Length > 0)
                    {
                        if (lsPers[1].Trim() == sFil)
                        {
                            sw.WriteLine(o.dtr1.sBadnum + ";" + lsPers[2] + ";" + lsPers[3] + ";" + lsPers[4] + ";" + lsPers[0] + ";" + lsPers[1] + ";" + o.dtr1.Start.ToString("yyyyMMdd"));
                        }
                    }
                    else
                    {
                        sw.WriteLine(o.dtr1.sBadnum + ";" + lsPers[2] + ";" + lsPers[3] + ";" + lsPers[4] + ";" + lsPers[0] + ";" + lsPers[1] + ";" + o.dtr1.Start.ToString("yyyyMMdd"));
                    }
                }
                sw.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            finally
            {
                Algemeen.Proces.VerwijderWrkFiles();
            }
        }
    }
    public class Overlap
    {
        public DateTimeRange dtr1;
        public DateTimeRange dtr2;

    }
    public class DateTimeRange
    {
        public string sBadnum;
        public DateTime Start { get; set; }
        public DateTime End { get; set; }
        public string sRecNr;

        public bool Intersects(DateTimeRange test)
        {
            if (this.Start > this.End || test.Start > test.End)
                throw new Exception();

            if (this.Start == this.End || test.Start == test.End)
                return false; // No actual date range

            if ((this.Start == test.Start || this.End == test.End)&&this.sBadnum == test.sBadnum && this.sRecNr != test.sRecNr)
                return true; // If any set is the same time, then by default there must be some overlap. 

            if (this.Start < test.Start)
            {
                if ((this.End > test.Start && this.End < test.End)&&this.sBadnum == test.sBadnum && this.sRecNr != test.sRecNr)
                    return true; // Condition 1

                if ((this.End > test.End) && this.sBadnum == test.sBadnum && this.sRecNr != test.sRecNr)
                    return true; // Condition 3
            }
            else
            {
                if ((test.End > this.Start && test.End < this.End) && this.sBadnum == test.sBadnum && this.sRecNr != test.sRecNr)
                    return true; // Condition 2

                if ((test.End > this.End) && this.sBadnum == test.sBadnum && this.sRecNr != test.sRecNr)
                    return true; // Condition 4
            }

            return false;
        }
    }
    class projRegi
    {
        public string sBadnum;
        public DateTime dtDateTime;
        public TimeSpan tsTime;
        public string sWplekkod;
        public TimeSpan tsPause;
        public string sActkod;
        public string sRecNr;
    }
}
